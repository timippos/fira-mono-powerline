#Fira Mono for Powerline

The more widely distributed version of Fira Mono with patched powerline symbols lacks correct Greek-Extended unicode
support, so I made my own patched version based on a more up-to-date version of it.

Fira is a reserved font name of the Mozilla Foundation and licensed under the OFL. The original font can be found
[here](https://github.com/mozilla/Fira).
